This is the Debian GNU/Linux r-cran-manipulatewidgets package of
manipulateWidgets.  The manipulateWidgets package provides tools to
add more interactivity to interactive charts. It was written by
Jalal-Edine Zawam, Francois Guillem, JJ Allaire, Marion Praz, Benoit
Thieurmel, Titouan Robert and Duncan Murdoch.

This package was created by Dirk Eddelbuettel <edd@debian.org>.
The sources were downloaded from the main CRAN site
	http://cran.r-project.org/src/contrib/
and are also available from all CRAN mirrors as e.g.
	http://cran.us.r-project.org/src/contrib/

The package was renamed from its upstream name 'manipulateWidgets' to
'r-cran-manipulatewidgets' to fit the pattern of CRAN (and non-CRAN)
packages for R.

Files: *
Copyright: 2017 - 2018  RTE
License: GPL-2+

Files: debian/*
Copyright: 2018  Dirk Eddelbuettel <edd@debian.org>
License: GPL-2+

On a Debian GNU/Linux system, the GPL license (version 2) is included
in the file /usr/share/common-licenses/GPL-2 and the GPL license
(version 3) is included in the file /usr/share/common-licenses/GPL-3

For reference, the upstream DESCRIPTION file is included below:

   Package: manipulateWidget
   Type: Package
   Title: Add Even More Interactivity to Interactive Charts
   Version: 0.9.0
   Date: 2018-03-26
   Authors@R: c(
       person("Jalal-Edine", "ZAWAM", email = "jalal-edine.zawam@rte-france.com", role = c("aut", "cre")                                                                              ),
       person("Francois", "Guillem", email = "francois.guillem@rte-france.com", role = c("aut")),
       person("RTE", role = "cph"),
       person("JJ", "Allaire", role = "ctb"),
   	  person("Marion", "Praz", email="mnpraz@gmail.com", role = "ctb", comment = "New user interface"),
   	  person("Benoit", "Thieurmel", role = "ctb", email = "benoit.thieurmel@datastorm.fr"),
   	  person(given = "Titouan", family = "Robert", email = "titouan.robert@datastorm.fr", role = "ctb"),
   	  person("Duncan", "Murdoch", email = "murdoch.duncan@gmail.com", role = "ctb")
       )
   Description: Like package 'manipulate' does for static graphics, this package
       helps to easily add controls like sliders, pickers, checkboxes, etc. that 
       can be used to modify the input data or the parameters of an interactive 
       chart created with package 'htmlwidgets'.
   URL: https://github.com/rte-antares-rpackage/manipulateWidget
   BugReports: https://goo.gl/pV7o5c
   License: GPL (>= 2) | file LICENSE
   Imports: shiny (>= 1.0.3), miniUI, htmltools, htmlwidgets, knitr,
           methods, tools, base64enc, grDevices, codetools
   Suggests: dygraphs, leaflet, plotly, xts, rmarkdown, testthat, covr
   LazyData: TRUE
   RoxygenNote: 6.0.1
   VignetteBuilder: knitr
   Encoding: UTF-8
   NeedsCompilation: no
   Packaged: 2018-03-26 15:02:06 UTC; jalazawa
   Author: Jalal-Edine ZAWAM [aut, cre],
     Francois Guillem [aut],
     RTE [cph],
     JJ Allaire [ctb],
     Marion Praz [ctb] (New user interface),
     Benoit Thieurmel [ctb],
     Titouan Robert [ctb],
     Duncan Murdoch [ctb]
   Maintainer: Jalal-Edine ZAWAM <jalal-edine.zawam@rte-france.com>
   Repository: CRAN
   Date/Publication: 2018-03-26 15:15:56 UTC

